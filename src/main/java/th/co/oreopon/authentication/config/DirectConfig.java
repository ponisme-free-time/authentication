package th.co.oreopon.authentication.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.support.converter.MessageConverter;

@Configuration
public class DirectConfig {
    @Value("${rabbitmq.queue.name}")
    private String queueVl;

    @Value("${rabbitmq.exchange.name}")
    private String exchangeVl;

    @Value("${rabbitmq.routing.key}")
    private String routingKeyVl;

    @Bean
    public Queue queue(){
        return new Queue(queueVl);
    }

    @Bean
    public DirectExchange exchange(){
        return new DirectExchange(exchangeVl);
    }

    @Bean
    public Binding binding(Queue queue, DirectExchange exchange){
        return BindingBuilder
                .bind(queue)
                .to(exchange)
                .with(routingKeyVl);
    }

    @Bean
    public MessageConverter converter(){
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory){
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }
}
