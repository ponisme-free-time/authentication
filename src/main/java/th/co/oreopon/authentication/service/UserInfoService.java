package th.co.oreopon.authentication.service;

import lombok.NoArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import th.co.oreopon.authentication.entity.UserDetail;
import th.co.oreopon.authentication.model.UserInfoDetails;
import th.co.oreopon.authentication.repository.UserDetailRepository;

import java.util.Optional;

@Service
@NoArgsConstructor
public class UserInfoService implements UserDetailsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserInfoService.class);

    private UserDetailRepository userDetailRepository;
    @Autowired
    public UserInfoService( UserDetailRepository userDetailRepository) {
        this.userDetailRepository=userDetailRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserDetail>userDetail=userDetailRepository.findByUsername(username);

        return userDetail.map(UserInfoDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("User not found " + username));
    }




}
