package th.co.oreopon.authentication.service;

import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import th.co.oreopon.authentication.entity.UserDetail;
import th.co.oreopon.authentication.model.UserInfo;
import th.co.oreopon.authentication.repository.UserDetailRepository;

import java.util.UUID;

@Service
public class UserDetailService {
    private final UserDetailRepository userDetailRepository;
    private final PasswordEncoder passwordEncoder;

    public UserDetailService(UserDetailRepository userDetailRepository, PasswordEncoder passwordEncoder) {
        this.userDetailRepository = userDetailRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public ResponseEntity<String> registerUser(UserInfo userInfo) {
        UserDetail userDetail = new UserDetail();
        try {
            PropertyUtils.copyProperties(userDetail, userInfo);
        }catch (Exception e){
            e.printStackTrace();
        }
        userDetail.setUserId("user_"+ UUID.randomUUID().toString());
        userDetail.setPassword(passwordEncoder.encode(userInfo.getPassword()));
        userDetailRepository.save(userDetail);
        return new ResponseEntity<>("", HttpStatus.OK);
    }

}
