package th.co.oreopon.authentication.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserInfo {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String roles;
}
