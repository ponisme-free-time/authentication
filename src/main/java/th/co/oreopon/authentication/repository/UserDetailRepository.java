package th.co.oreopon.authentication.repository;

import org.springframework.data.repository.CrudRepository;
import th.co.oreopon.authentication.entity.UserDetail;

import java.util.Optional;

public interface UserDetailRepository extends CrudRepository<UserDetail,String> {

    Optional<UserDetail> findByUsername(String username);
}
