package th.co.oreopon.authentication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import th.co.oreopon.authentication.model.Login;
import th.co.oreopon.authentication.model.UserInfo;
import th.co.oreopon.authentication.service.UserDetailService;
import th.co.oreopon.authentication.util.RabbitMQProducer;

@RestController
@RequestMapping("/api/authentication")
public class AuthenticationController {
    @Autowired
    private UserDetailService userDetailService;
    @Autowired
    private RabbitMQProducer rabbitMQProducer;
    @GetMapping("/welcome")
    public String welcome(){
        return "Welcome to my application";
    }

    @PostMapping("/register")
    public ResponseEntity<String> addNewUser(@RequestBody UserInfo user){
        return userDetailService.registerUser(user);
    }
    @GetMapping("/testmq")
    public void testMQ(){
        Login data = new Login();
        data.setUsername("TEST");
        data.setPassword("TEST");
        rabbitMQProducer.sendJsonMessage(data);
    }
}
