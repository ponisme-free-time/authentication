package th.co.oreopon.authentication.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;


@Entity
@Data
public class UserDetail {
    @Id
    private String userId;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String roles;


}
